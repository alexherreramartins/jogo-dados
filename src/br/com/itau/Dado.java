package br.com.itau;

public class Dado {


    private int tamanho;

    public Dado(int tamanho){
        this.tamanho = tamanho;
    }

    public int getTamanho() {
        return tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }


}
