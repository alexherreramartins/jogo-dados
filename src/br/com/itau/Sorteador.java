package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Sorteador {

    public static List<Resultado>  obterValor(Dado dado, int jogadas, int intervalo, int grupo){

        List<Resultado> saida = new ArrayList<Resultado>();

        for (int i = 0; i<jogadas; i++){
            saida.add(new Resultado(grupo ,(int) (Math.random() * intervalo + 1)));
        }
        return saida;
    }

    public static List<Resultado>  obterValorGrupos(Dado dado, int jogadas, int intervalo, int rodadas){
        List<Resultado> saida = new ArrayList<Resultado>();

        for (int j=0;j<rodadas;j++){
            for (int i=0;i<jogadas;i++){
                saida.add(new Resultado(j ,(int) (Math.random() * intervalo + 1)));
            }
        }

        return saida;
    }
}
