package br.com.itau;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class IO {

    public static void imprimeValor(List<Resultado> vetor){
        String saida = "";

        for (int i=0 ; i < vetor.size() ;i++){
            if (i+1 == vetor.size()) {
                saida += vetor.get(i).getValor() ;
            }else{
                saida += vetor.get(i).getValor() + ",";
            }
        }
        System.out.println(saida);
    }

    public static void imprimeValorSoma(List<Resultado> resultados, int grupo){
        String saida = "";

        Predicate<Resultado> byGrupo = resultado -> resultado.getGrupo() == grupo;
        resultados  = resultados.stream().filter(byGrupo).collect(Collectors.toList());

        int soma = Resultado.somarValor(resultados);

        for (int i=0 ; i < resultados.size() ;i++){
            if (i+1 == resultados.size()) {
                saida += resultados.get(i).getValor() + "," + soma;
            }else{
                saida += resultados.get(i).getValor() + ",";
            }
        }


        System.out.println(saida);
    }

    public static void imprimeValorSomaAll(List<Resultado> resultados, int grupos){

        for (int i=0;i<grupos;i++){
            imprimeValorSoma(resultados,i);
        }

    }
}
