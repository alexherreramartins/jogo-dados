package br.com.itau;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Resultado {
    private int grupo;
    private int valor;

    public Resultado(int grupo, int valor){
        this.grupo = grupo;
        this.valor = valor;
    }
    public int getGrupo() {
        return grupo;
    }

    public void setGrupo(int grupo) {
        this.grupo = grupo;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public static int somarValor(List<Resultado> resultados){

        int soma = 0;
        for (Resultado result : resultados){
            soma += result.getValor();
        }

        return soma;
    }

}
