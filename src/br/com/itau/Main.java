package br.com.itau;

public class Main {

    public static void main(String[] args) {

        Dado dado =  new Dado(6);

        System.out.println("1o passo");
        IO.imprimeValor(Sorteador.obterValor(dado,1,dado.getTamanho(),1));

        System.out.println("2o passo");
        IO.imprimeValorSoma(Sorteador.obterValorGrupos(dado,3,dado.getTamanho(),1 ),0);

        System.out.println("3o passo");
        IO.imprimeValorSomaAll(Sorteador.obterValorGrupos(dado,3,dado.getTamanho(),3),3);
    }
}
